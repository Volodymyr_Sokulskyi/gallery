<!doctype html>
<html lang="en">
<head>
    <title>Upload images</title>
    <script
        src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Something went wrong.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <h3 class="jumbotron">Upload images</h3>
    <a class="btn btn-default" href="{{route('gallery')}}">< Gallery</a>
    <form id="image_form" method="post" action="{{route('upload-images')}}" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="input-group control-group">
            <div class="form-group">
                <label>Image</label>
                <input type="file" name="images[]" class="form-control" accept="image/*">
            </div>
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="titles[]" class="form-control">
            </div>
            <div class="form-group">
                <label>Tags</label>
                <select size="5" name="tags[0][]" class="form-control" multiple>
                    <option disabled selected>Choose tags</option>
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button id="add_new_image" class="btn btn-success" type="button" style="margin-top:10px">
            <i class="glyphicon glyphicon-plus"></i>Add
        </button>

        <button type="submit" class="btn btn-primary" style="margin-top:10px">Submit</button>
    </form>

    <div class="clone hide">
        <div class="input-group control-group">
            <div class="form-group">
                <label>Image</label>
                <input type="file" name="images[]" class="form-control" accept="image/*">
            </div>
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="titles[]" class="form-control">
            </div>
            <div class="form-group">
                <label>Tags</label>
                <select size="5" name="tags[][]" class="form-control" multiple>
                    <option disabled selected>Choose tags</option>
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#add_new_image").click(function () {
            var html = $(".clone").html();

            $("#add_new_image").before(html);

            var countImages = $("#image_form").children().length - 3;

            $("form select").last().attr('name', 'tags[' + (countImages - 1) + '][]');
        });

        $("body").on("click", ".btn-danger", function () {
            $(this).parents(".control-group").remove();
        });
    });

</script>

<style>
    .form-group {
        margin-bottom: 50px;
    }

    .input-group {
        margin-top: 20px;
        border: 1px solid #000;
        padding: 10px;
    }
</style>
</body>
</html>
