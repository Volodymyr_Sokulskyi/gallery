<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gallery</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script
        src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        .gallery-title {
            font-size: 36px;
            color: #42B32F;
            text-align: center;
            font-weight: 500;
        }

        .gallery-title:after {
            content: "";
            position: absolute;
            width: 7.5%;
            left: 46.5%;
            height: 45px;
            border-bottom: 1px solid #5e5e5e;
        }

        .filter-button, .upload-button {
            font-size: 18px;
            border: 1px solid #42B32F;
            border-radius: 5px;
            text-align: center;
            color: #42B32F;
            margin-bottom: 30px;

        }

        .filter-button:hover, .upload-button:hover {
            font-size: 18px;
            border: 1px solid #42B32F;
            border-radius: 5px;
            text-align: center;
            color: #ffffff;
            background-color: #42B32F;

        }

        .btn-default:active .filter-button:active {
            background-color: #42B32F;
            color: white;
        }

        .port-image {
            width: 100%;
        }

        .gallery_product {
            margin-bottom: 30px;
            width: 365px;
            height: 365px;
            object-fit: cover;
        }

        .desc {
            padding: 5px;
            text-align: center;
            font-size: 90%;
            border: 1px solid #42B32F;
        }

        .hovereffect {
            width: 100%;
            height: 100%;
            float: left;
            overflow: hidden;
            position: relative;
            text-align: center;
            cursor: default;
        }

        .hovereffect .overlay {
            width: 100%;
            position: absolute;
            overflow: hidden;
            left: 0;
            top: auto;
            bottom: 0;
            background: #79FAC4;
            color: #3c4a50;
            -webkit-transition: -webkit-transform 0.35s;
            transition: transform 0.35s;
            -webkit-transform: translate3d(0, 100%, 0);
            transform: translate3d(0, 100%, 0);
            visibility: hidden;

        }

        .hovereffect img {
            display: block;
            position: relative;
            -webkit-transition: -webkit-transform 0.35s;
            transition: transform 0.35s;
            height: 100%;
            object-fit: cover;
        }

        .hovereffect:hover img {
            -webkit-transform: translate3d(0, -3em, 0);
            transform: translate3d(0, -3em, 0);
        }

        .hovereffect h2 {
            color: #fff;
            text-align: center;
            position: relative;
            font-size: 17px;
            padding: 0.75em;
            background: rgba(0, 0, 0, 0.6);
            margin: 0px;
        }

        .hovereffect a.info {
            display: inline-block;
            text-decoration: none;
            padding: 7px 14px;
            text-transform: uppercase;
            color: #fff;
            border: 1px solid #fff;
            margin: 50px 0 0 0;
            background-color: transparent;
        }

        .hovereffect a.info:hover {
            box-shadow: 0 0 5px #fff;
        }


        .hovereffect p.icon-links a {
            float: right;
            color: #3c4a50;
            font-size: 1.4em;
        }

        .hovereffect:hover p.icon-links a:hover,
        .hovereffect:hover p.icon-links a:focus {
            color: #252d31;
        }

        .hovereffect h2,
        .hovereffect p.icon-links a {
            -webkit-transition: -webkit-transform 0.35s;
            transition: transform 0.35s;
            -webkit-transform: translate3d(0, 200%, 0);
            transform: translate3d(0, 200%, 0);
            visibility: visible;
        }

        .hovereffect p.icon-links a span:before {
            display: inline-block;
            padding: 8px 10px;
            speak: none;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }


        .hovereffect:hover .overlay,
        .hovereffect:hover h2,
        .hovereffect:hover p.icon-links a {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        .hovereffect:hover h2 {
            -webkit-transition-delay: 0.05s;
            transition-delay: 0.05s;
        }

        .hovereffect:hover p.icon-links a:nth-child(3) {
            -webkit-transition-delay: 0.1s;
            transition-delay: 0.1s;
        }

        .hovereffect:hover p.icon-links a:nth-child(2) {
            -webkit-transition-delay: 0.15s;
            transition-delay: 0.15s;
        }

        .hovereffect:hover p.icon-links a:first-child {
            -webkit-transition-delay: 0.2s;
            transition-delay: 0.2s;
        }

        .images {
            margin-top: 30px;
        }

        .pagination {
            display: table;
            margin: 10px auto;
        }
    </style>
</head>
<body>
<section>
    <div class="container">
        <div class="row">
            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1 class="gallery-title">Gallery</h1>
            </div>
            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
                <a class="btn btn-default upload-button" href="{{route('upload-images')}}">Upload image</a>
            </div>
        </div>
        <div class="row images">
            <div align="center">
                <button class="btn btn-default filter-button" data-filter="all">All</button>
                @foreach($tags as $tag)
                    <button class="btn btn-default filter-button" data-filter="{{$tag->id}}">{{$tag->name}}</button>
                @endforeach
            </div>
            <br/>

            @foreach($images as $image)
                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                    <div class="hovereffect">
                        <img src="{{asset($image->path)}}" class="img-responsive">
                        <div class="overlay">
                            <h2>{{$image->title}}</h2>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<div class="pagination">
    {{ $images->links() }}
</div>
<script>
    $(document).ready(function () {

        const urlParams = new URLSearchParams(window.location.search);
        const tags = urlParams.getAll('tags[]')
        for(const tag of tags) {
            ($(".filter-button[data-filter="+tag+"]").addClass('active'))
        }

        $(".filter-button").click(function () {
            if($(this).hasClass('active'))
                $(this).removeClass('active')
            else
                $(this).addClass('active')


            if ($(this).attr('data-filter') == "all")
                $(".filter-button").removeClass('active')


            var filters = '/?';

            $( ".filter-button.active" ).each(function() {
                filters+='tags[]='+$( this ).attr('data-filter')+'&';
            });

            window.location = filters;

        });
    });
</script>
</body>
</html>
