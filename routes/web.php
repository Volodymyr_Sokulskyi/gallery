<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'GalleryController@index')->name('gallery');

Route::get('upload-images', 'UploadImageController@index')->name('upload-images');
Route::post('upload-images', 'UploadImageController@store');
