<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Tag
 *
 * @property string $name
 *
 * @package App\Models
 */
class Tag extends Model
{
    protected $fillable = [
        'name'
    ];

    public function images(): BelongsTo
    {
        return $this->belongsTo(Image::class);
    }
}
