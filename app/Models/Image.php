<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ImageService
 *
 * @property string $title
 * @property string $name
 * @property string $path
 *
 * @package App\Models
 */
class Image extends Model
{
    protected $fillable = [
        'title',
        'name',
        'path',
    ];

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }
}
