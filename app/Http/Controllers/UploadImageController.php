<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadImageRequest;
use App\Models\Tag;
use App\Services\Image\ImageService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class UploadImageController
 *
 * @package App\Http\Controllers
 */
class UploadImageController extends Controller
{
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * UploadImageController constructor.
     *
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * Show upload image form
     *
     * @return View
     */
    public function index(): View
    {
        return view('images', ['tags' => Tag::all()]);
    }

    /**
     * Store multiply images
     *
     * @param UploadImageRequest $request
     *
     * @return RedirectResponse
     */
    public function store(UploadImageRequest $request): RedirectResponse
    {
        $this->imageService->uploadMultiplyImages($request->validated());

        return back()->with('success', 'Images uploaded successfully');
    }
}
