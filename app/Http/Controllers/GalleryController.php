<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class GalleryController
 *
 * @package App\Http\Controllers
 */
class GalleryController extends Controller
{
    /**
     * Show all images with pagination and filtered by tags
     *
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request): View
    {
        //TODO: need create repository
        $images = Image::when(request('tags'), function ($q) use ($request){
            $q->whereHas('tags', function ($query) use ($request) {
                $query->whereIn('id', $request->get('tags'));
            });
        })->orderByDesc('id')->paginate(9);

        return view('gallery', [
            'tags' => Tag::all(),
            'images' => $images
        ]);
    }
}
