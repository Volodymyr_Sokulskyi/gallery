<?php

namespace App\Http\Requests;

use App\Models\Tag;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UploadImageRequest
 *
 * @package App\Http\Requests
 */
class UploadImageRequest extends FormRequest
{
    /**
     * @{inheritDoc}
     */
    public function rules(): array
    {
        return [
            'images' => 'required',
            'titles.*' => 'required|string',
            'tags' => 'required',
            'tags.*.*' => 'exists:' . Tag::class . ',id',
        ];
    }
}
