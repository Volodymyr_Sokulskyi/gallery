<?php

namespace App\Services\Image;

/**
 * Interface ImageService
 *
 * @package App\Services\ImageService
 */
interface ImageService
{
    /**
     * Upload multiply images
     *
     * @param array $data
     *
     * @return void
     */
    public function uploadMultiplyImages(array $data): void;
}
