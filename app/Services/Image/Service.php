<?php

namespace App\Services\Image;

use App\Models\Image;
use Illuminate\Support\Str;

/**
 * Class Service
 *
 * @package App\Services\Image
 */
class Service implements ImageService
{
    /**
     * @inheritDoc
     */
    public function uploadMultiplyImages(array $data): void
    {
        foreach ($data['images'] as $key => $image) {
            $name = Str::uuid() . '.' . $image->getClientOriginalExtension();
            $path = $image->storeAs('uploads', $name, 'public');

            $image = Image::create([
                'name' => $name,
                'title' => $data['titles'][$key],
                'path' => '/storage/' . $path
            ]);

            $image->tags()->attach($data['tags'][$key]);
        }
    }
}
