<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

/**
 * Class TagsTableSeeder
 */
class TagsTableSeeder extends Seeder
{
    /**
     * @{inheritDoc}
     */
    public function run(): void
    {
        \DB::table('tags')->truncate();

        Tag::insert([
            ['name' => 'Nature'],
            ['name' => 'Cars'],
            ['name' => 'Robots'],
            ['name' => 'Mountains'],
            ['name' => 'Technologies'],
            ['name' => 'Sea'],
            ['name' => 'Forest'],
            ['name' => 'Desert'],
            ['name' => 'Eat'],
            ['name' => 'Fruit'],
            ['name' => 'Vegetables'],
        ]);
    }
}
