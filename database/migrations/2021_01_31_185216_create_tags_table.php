<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    /**
     * @{inheritDoc}
     */
    public function up(): void
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);
        });
    }

    /**
     * @{inheritDoc}
     */
    public function down(): void
    {
        Schema::dropIfExists('tags');
    }
}
