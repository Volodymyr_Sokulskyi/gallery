<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * @{inheritDoc}
     */
    public function up(): void
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('name');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * @{inheritDoc}
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
