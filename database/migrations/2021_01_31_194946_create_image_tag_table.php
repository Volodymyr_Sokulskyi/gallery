<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageTagTable extends Migration
{
    /**
     * @{inheritDoc}
     */
    public function up(): void
    {
        Schema::create('image_tag', function (Blueprint $table) {
            $table->unsignedBigInteger('image_id');
            $table->unsignedBigInteger('tag_id');

            $table->foreign('image_id')
                ->on('images')
                ->references('id')
                ->onDelete('cascade');

            $table->foreign('tag_id')
                ->on('tags')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * @{inheritDoc}
     */
    public function down(): void
    {
        Schema::dropIfExists('image_tag');
    }
}
